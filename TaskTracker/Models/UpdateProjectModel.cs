﻿namespace TaskTracker.Models
{
    public class UpdateProjectModel
    {
        public bool IsPrivate { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public int CurrentStatus { get; set; }
    }
}
