﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Models
{
    public class AppUserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string? Name { get; set; }
        public string? PictureUrl { get; set; }
    }

    public class AppRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual int WorkspaceId { get; set; }
        public virtual RolePermissionModel Permission { get; set; }

    }

    public class RolePermissionModel
    {
        public int Id { get; set; }
        public bool CreateTask { get; set; }
        public bool EditTask { get; set; }
        public bool DeleteTask { get; set; }
        public bool CreateProject { get; set; }
        public bool EditProject { get; set; }
        public bool DeleteProject { get; set; }
        public bool AddWorkspaceUser { get; set; }
        public bool RemoveWorkspaceUser { get; set; }
    }
}
