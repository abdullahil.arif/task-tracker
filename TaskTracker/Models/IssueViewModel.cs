﻿namespace TaskTracker.Models
{
    public class IssueFieldValues
    {
        public List<IssueStatusModel> Statuses { get; set; }
        public List<IssuePriorityModel> Priorities { get; set; }
        public List<AppUserModel> Users { get; set; }
    }

    public class EditIssueViewModel
    {
        public IssueModel Issue { get; set; }
        public IssueFieldValues FieldValues { get; set; }
    }

    public class CreateIssueViewModel
    {
        public int ProjectId { get; set; }
        public IssueFieldValues FieldValues { get; set; }
    }
}
