﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Models
{
    public class WorkspaceMemberModel
    {
        public int Id { get; set; }
        public int WorkspaceId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public virtual WorkspaceModel Workspace { get; set; }
        public virtual AppUserModel User { get; set; }
        public virtual string RoleId { get; set; }
    }
}
