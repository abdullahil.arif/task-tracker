﻿namespace TaskTracker.Models
{
    public class DashboardViewModel
    {
        public List<ProjectModel> Projects { get; set; }
    }
}
