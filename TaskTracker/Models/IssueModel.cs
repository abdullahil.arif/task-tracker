﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracker.Models
{
    public class IssueModel
    {
        public int Id { get; set; } 
        public int ProjectId { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public int? IssueType { get; set; }
        public int? PriorityId { get; set; }
        public int? StatusId { get; set; }
        public DateTime DueDate { get; set; }
        public string? AssigneeId { get; set; }
        public string CreatedById { get; set; }
        public AppUserModel Assignee { get; set; }
        public AppUserModel CreatedBy { get; set; }
        public IssuePriorityModel Priority { get; set; }
        public IssueStatusModel Status { get; set; }
        public List<CommentModel> Comments { get; set; }
    }

    public class CommentModel
    {
        public int Id { get; set; } 
        public int IssueId { get; set; }
        public string CommentText { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string CreatedById { get; set; }
        public virtual AppUserModel CreatedBy { get; set; }
    }

    public class IssueStatusModel
    {
        public int WorkspaceId { get; set; }
        public int Id { get; set; }
        public int ProjectId { get; set; }
        [Required]
        public string Description { get; set; }
        public string ColorCode { get; set; }
    }

    public class IssuePriorityModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
    }
}