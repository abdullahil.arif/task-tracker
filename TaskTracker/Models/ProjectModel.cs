﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public DateTime EndDate { get; set; }
        public int CurrentStatus { get; set; } // Active, Completed, Archived
        public string CreatedById { get; set; }
        public int WorkspaceId { get; set; }
        public bool IsPrivate { get; set; }
        public string? ColorCode { get; set; }
        public string? Icon { get; set; }

        public List<IssueModel> Issues { get; set; }
        public List<IssueStatusModel> Statuses { get; set; }
        public List<IssuePriorityModel> Priorities { get; set; }
    }
}
