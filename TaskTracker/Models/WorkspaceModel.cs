﻿using TaskTracker.Core.Domain;
using TaskTracker.Core.Domain.Workspaces;

namespace TaskTracker.Models
{
    public class WorkspaceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedById { get; set; }
        public AppUserModel CreatedBy { get; set; }
        public virtual ICollection<WorkspaceMemberModel> WorkspaceMembers { get; set; }
        public virtual ICollection<AppRoleModel> WorkspaceRoles { get; set; }
    }
}
