﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracker.Models
{
    public class CreateIssueModel
    {
        public int WorkspaceId { get; set; }
        public int ProjectId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        public string Description { get; set; } = "";
        public int IssueType { get; set; }
        public int PriorityId { get; set; }
        public int StatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public string AssigneeId { get; set; }
    }
}
