﻿namespace TaskTracker.Models
{
    public class BaseViewModel
    {

        public AppUserModel User { get; set; }
        public WorkspaceModel Workspace { get; set; }
        public List<ProjectModel> Projects { get; set; }
        public RolePermissionModel Permissions { get; set; }
    }
}
