﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracker.Models.Account
{
    public class PasswordUpdateModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(255, ErrorMessage = "Must be atleast 8 characters", MinimumLength = 8)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "Password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
