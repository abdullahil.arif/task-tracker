﻿using TaskTracker.Models.Account;

namespace TaskTracker.Models
{
    public class UserProfileViewModel
    {
        public UserProfileViewModel()
        {
            PasswordUpdate = new PasswordUpdateModel();
        }
        public AppUserModel User { get; set; }
        public PasswordUpdateModel PasswordUpdate { get; set; }
    }
}
