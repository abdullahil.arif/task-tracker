﻿var toastr;

$(document).ready(() => {
    toastr = new Notyf();
    refreshDatePicker('body');
    $(document).on('input', '.custom-text-update .form-control', function () {
        $(this).addClass('modified');
    })
    $('button.no-access').attr('title', 'You are not authorized to perform this action.');
});

// #region Common
function refreshDatePicker(container) {
    $(container).find('.jquery-date-picker').datepicker({ dateFormat: 'dd MM yy' });
}
function validateAndSubmit(form) {
    let isValid = true;

    // required field validation
    $(form).find('.custom-validation[data-required="true"]').each(function (index, element) {
        let val = $(element).val().trim();
        if (val == '') {
            toastr.error('Please enter all required fields.');
            isValid = false;
            return false;
        }
    });

    if (isValid) $(form).submit();
}

function showNotificationAndRedirect(message, url, timeout = 1000) {
    showOverlay();
    toastr.success(message);
    setTimeout(() => {
        window.location.href = url;
    }, timeout)
}
function showOverlay() {
    $('.overlay').show();
}
function getCurrentWorkspaceId() {
    try {
        let workspaceId = $('.current-workspace').data('id');
        return parseInt(workspaceId);
    } catch (ex) {
        return 0;
    }
}
function getCurrentProjectId() {
    try {
        let workspaceId = $('.current-project').data('id');
        return parseInt(workspaceId);
    } catch (ex) {
        return 0;
    }
}
// #endregion

// #region Project
function openCreateProjectModal() {
    $('#create-project-modal').modal('show');
}

function onProjectCreated(resp) {
    if (!resp.isSuccess) {
        toastr.error(resp.errorMessage);
        return;
    }
    else {
        $('#create-project-modal').modal('hide');
        showNotificationAndRedirect('Project Created.', '/project/index/' + resp.data);
    }
}
function onProjectUpdated(resp) {
    if (!resp.isSuccess) {
        toastr.error(resp.errorMessage);
        return;
    }
    else {
        toastr.success('Project Information Updated.');
    }
}
function deleteProject(id) {
    if (confirm('Are you sure?')) {
        $.post('/Project/Delete', { id }).done((resp) => {
            if (resp.isSuccess) {
                showNotificationAndRedirect('Project Deleted.', '/');
            }
            else {
                toastr.error(resp.errorMessage);
            }
        })
    }
}
// #endregion

// #region Issue
function initCreateIssue(projectId) {
    $('.modal-container').load('/Issue/CreateIssue?projectId=' + projectId, function (resp, status, xhr) {
        if (status === 'error') {
            toastr.error('An error occurred. Please try again.');
        } else {
            refreshDatePicker('#create-issue-modal');
            $('#create-issue-modal input#workspace-id').val(getCurrentWorkspaceId());
            $('#create-issue-modal').modal('show');
        }
    });
}
function onIssueCreated(resp) {
    if (resp.isSuccess) {
        toastr.success('Task Created.');
        let statusGroup = $(resp.data).data('status');
        $(`.task-table tbody.section-body[data-status="${statusGroup}"]`).append($(resp.data));
        $('#create-issue-modal').modal('hide');
        updateTaskCount();
    }
    else {
        toastr.error(resp.errorMessage);
    }
}
function editIssue(id) {
    $('.issue-preview-container').load(`/Issue/Preview?workspaceId=${getCurrentWorkspaceId()}&issueId=${id}`, function (resp, status, xhr) {
        if (status === 'error') {
            toastr.error('An error occurred. Please try again.');
        } else {
            refreshDatePicker('#issue-preview');
            $('#issue-preview').offcanvas('show');
        }
    });
}
function deleteIssue(id) {
    if (window.confirm('Are you sure?')) {
        $.post('/Issue/Delete', { id }).done((resp) => {
            if (resp.isSuccess) {
                toastr.success('Task Deleted.');
                $(`.task-table tr[data-id="${id}"]`).remove();
                updateTaskCount();
            }
            else {
                toastr.error(resp.errorMessage);
            }
        })
    }
}

function updateIssueDescription(id, obj) {
    if ($(obj).hasClass('modified')) {
        $(obj).removeClass('modified');
        updateIssueProperty(id, 'Description', $(obj).val().trim());
    }
}
function updateIssueTitle(id, obj) {
    if ($(obj).val().trim() == '') return;
    if ($(obj).hasClass('modified')) {
        $(obj).removeClass('modified');
        updateIssueProperty(id, 'Title', $(obj).val().trim());
    }
}
function updateIssueAssignee(id, userId, obj) {
    $('.issue-preview-assignee img').attr('src', $(obj).find('img').attr('src'));
    $('.issue-preview-assignee .name').text($(obj).find('.name').text());
    updateIssueProperty(id, 'Assignee', userId);
}


function updateIssueProperty(id, fieldName, value) {
    $.post('/Issue/UpdateProperty', { id, fieldName, value }).done((resp) => {
        if (resp.isSuccess) {
            toastr.success(fieldName + ' Updated.');
            refreshIssue(id);
        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}
function refreshIssue(id) {
    $.get(`/Issue/Details?workspaceId=${getCurrentWorkspaceId()}&issueId=${id}`).done((resp) => {
        if (resp.isSuccess) {
            $(`.task-table tbody.section-body tr[data-id="${id}"]`).remove();
            var issueStatusId = $(resp.data).data('status');
            $(resp.data).appendTo($(`.task-table tbody.section-body[data-status="${issueStatusId}"]`));
            updateTaskCount();
        }
    })
}
// #endregion

// #region Workspace
function inviteUser() {
    let email = $('#user-email-search').val();
    if (email.trim() == '') {
        toastr.error('Please enter email.');
        return false;
    }
    let workspaceId = getCurrentWorkspaceId();
    $.post('/Workspace/InviteUser', { workspaceId, email }).done((resp) => {
        console.log(resp);
        if (resp.isSuccess) {
            toastr.success('User Added.');
        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}
function getRolePermission(event) {
    $("#role-permission-table").load("/Workspace/GetRolePermission/" + event.target.value, function (resp, status, xhr) {
        if (status == 'error') {
            toastr.error('An error occurred');
        }

    });
}
function updateUserRole(userId, roleId) {
    let workspaceId = getCurrentWorkspaceId();
    $.post('/Workspace/UpdateUserRole', { workspaceId, userId, roleId }).done((resp) => {
        if (resp.isSuccess) {
            toastr.success('Access Updated.');
        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}

function onWorkspaceInfoUpdated(resp) {
    if (resp.isSuccess) {
        toastr.success('Info Updated.');
    }
    else {
        toastr.error(resp.errorMessage);
    }
}
function removeWorkspaceUser(workspaceId, userId) {
    if (window.confirm('Are you sure?')) {
        $.post('/Workspace/RemoveUser', { workspaceId, userId }).done((resp) => {
            if (resp.isSuccess) {
                toastr.success('User Removed.');
            }
            else {
                toastr.error(resp.errorMessage);
            }
        })
    }
}
// #endregion

// #region Customization
function editStatus(id) {
    $(".status-edit-modal-container").load("/Project/GetStatus/" + id, function (resp, status, xhr) {
        if (status == 'error') {
            toastr.error('An error occurred');
        }
        else {
            if (id == 0) {
                $('#status-edit-modal').find('#project-id').val(getCurrentProjectId());
            }
            $('#status-edit-modal').find('#workspace-id').val(getCurrentWorkspaceId());
            $('#status-edit-modal').modal('show');
        }
    });
}
function selectColor(colorCode, obj) {
    $('.color-chooser').removeClass('active');
    $(obj).addClass('active');
    $('#color-code').val(colorCode);
}
function onStatusUpdated(resp) {
    if (!resp.isSuccess) {
        toastr.error(resp.errorMessage);
        return;
    }
    else {
        let id = $(resp.data).data('id');
        if ($(`.status-list .list-group-item[data-id="${id}"]`).length > 0) {
            $(`.status-list .list-group-item[data-id="${id}"]`).replaceWith($(resp.data));
        }
        else {
            $(resp.data).appendTo($('.status-list'));
        }
        toastr.success('Status Updated.');
        $('#status-edit-modal').modal('hide');
    }
}
function deleteStatus(id) {
    if (window.confirm('Are you sure?')) {
        if ($('.status-list .list-group-item').length == 1) {
            toastr.error('Can\'t delete the last status');
            return;
        }
        $.post('/Project/DeleteStatus', { id }).done((resp) => {
            if (resp.isSuccess) {
                toastr.success('Status Deleted.');
                $(`.status-list .list-group-item[data-id="${id}"]`).remove();
            }
            else {
                toastr.error(resp.errorMessage);
            }
        })
    }
}
// #endregion 

// #region Comments
function editComment(id) {
    let text = $(`.comment-wrapper[data-id="${id}"] .comment-text`).text();
    let tempEditor = commentPreviewEditor(id, text);
    $(tempEditor).appendTo(`.comment-wrapper[data-id="${id}"] .comment-body`);
    $(`.comment-wrapper[data-id="${id}"] .comment-text`).hide();
    $(`.comment-wrapper[data-id="${id}"] .comment-edit-control`).hide();
}
function updateComment(commentId) {
    let text = $('.temp-editor').val();
    $.post('/Issue/UpdateComment', { commentId, text }).done((resp) => {
        if (resp.isSuccess) {
            $(`.comment-wrapper[data-id="${commentId}"] .comment-text`).text(text);
            discardComment(commentId);

        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}
function discardComment(id) {
    $(`.comment-update[data-id="${id}"]`).remove();
    $(`.comment-wrapper[data-id="${id}"] .comment-text`).show();
    $(`.comment-wrapper[data-id="${id}"] .comment-edit-control`).show();

}
function commentPreviewEditor(id, text) {
    return `<div class="comment-update my-3" data-id="${id}">
                <textarea class="form-control temp-editor" rows="4">${text}</textarea>
                <div class="custom-text-update-btn-group" role="group">
                    <button type="button" class="btn btn-sm bg-primary-subtle text-primary" onclick="updateComment(${id})"> Save </button>
                    <button type="button" class="btn btn-sm btn-light" onclick="discardComment(${id})"> Cancel </button>
                </div>
            </div>`;
}
function showCommentControls() {
    $('.comment-editor-buttons').show();
}
function clearCommentText() {
    $('.comment-editor-buttons').hide();
    $('#text-comment').val('');
}
function addComment(issueId) {
    let text = $('#text-comment').val();
    if (text.trim() == '') {
        toastr.error('Please enter a text.');
        return;
    }
    $.post('/Issue/AddComment', { issueId, text }).done((resp) => {
        if (resp.isSuccess) {
            $(resp.data).appendTo($('.comment-section'));
        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}
function deleteComment(commentId) {
    $.post('/Issue/DeleteComment', { commentId }).done((resp) => {
        if (resp.isSuccess) {
            $(`.comment-section .comment-wrapper[data-id="${commentId}"]`).remove();
        }
        else {
            toastr.error(resp.errorMessage);
        }
    })
}
// #endregion

//#region Profile
function showImageEditModal() {

    $('#edit-profile-picture-modal .img-preview').attr('src', $('.img-avatar').attr('src'));
    $('#edit-profile-picture-modal').modal('show');
}

function onProfilePictureSelected(event) {
    var input = event.target;
    var image = document.getElementById('profile-image-preview');
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            image.src = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function startProfilePictureSection() {
    $('#edit-profile-picture-modal #profile-image-chooser').trigger('click');

}

function onProfilePictureUpdated(response) {
    if (response.isSuccess) {
        toastr.success('Uploaded successfully.');
        $('#editProfilePictureModal').modal('hide');
        $('.profile-img-thumbnail').attr('src', response.data);
    }
    else {
        toastr.error(response.errorMessage);
    }
}

function onProfileUpdated(response) {
    if (response.isSuccess) {
        toastr.success('Updated successfully.');
    }
    else {
        toastr.error(response.errorMessage);
    }
}

function onPasswordUpdated(response) {
    if (response.isSuccess) {
        toastr.success('Updated successfully.');

        setTimeout(() => {
            window.location.reload();
        }, 1000);
    }
    else {
        toastr.error(response.errorMessage);
    }
}

function selectWorkspace() {
    $('.modal-container').load('/Workspace/GetAssignedWorkspaces', function (resp, status, xhr) {
        if (status === 'error') {
            toastr.error('An error occurred. Please try again.');
        } else {
            $('#switch-space-modal').modal('show');
        }
    });
}

function updateTaskCount() {
    $('tbody.section-header').each(function (index, element) {
        let val = $(element).data('status');
        let count = $(`tbody.section-body[data-status="${val}"] tr`).length;
        $(element).find('.issue-count').text(`(${count})`);
    });
}

function createWorkspace() {
    $('#create-workspace-modal').modal('show');
}

function onWorkspaceCreated(resp) {
    if (!resp.isSuccess) {
        toastr.error(resp.errorMessage);
        return;
    }
    else {
        $('#create-workspace-modal').modal('hide');
        showNotificationAndRedirect('Workspace Created.', '/workspace/index/' + resp.data);
    }
}

function deleteWorkspace(id) {
    if (confirm('Are you sure?')) {
        $.post('/Workspace/Delete', { id }).done((resp) => {
            if (resp.isSuccess) {
                showNotificationAndRedirect('Workspace Deleted.', '/');
            }
            else {
                toastr.error(resp.errorMessage);
            }
        })
    }
}
    // #endregion