﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Service.Services;

namespace TaskTracker
{
    public class WebWorkContext : IWorkContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<AppUser> _userManager;
        private readonly IWorkspaceService _workspaceService;
        public WebWorkContext(
            IHttpContextAccessor httpContextAccessor,
            UserManager<AppUser> userManager,
            IWorkspaceService workspaceService)
        {
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _workspaceService = workspaceService;
        }

        public string UserId => _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier) ?? string.Empty;
        public async Task<AppUser> GetCurrentUserAsync()
        {
            return await _userManager.FindByIdAsync(UserId);
        }

        public async Task<int> GetCurrentWorkspaceIdAsync()
        {
            int workspaceId = _httpContextAccessor.HttpContext?.Session.GetInt32("WorkspaceId") ?? -1;
            
            if(workspaceId == -1)
            {
                workspaceId = (await _workspaceService.GetDefaultWorkspaceAsync(UserId)).Id;
                _httpContextAccessor.HttpContext?.Session.SetInt32("WorkspaceId", workspaceId);
            }
            return workspaceId;
        }
    }
}
