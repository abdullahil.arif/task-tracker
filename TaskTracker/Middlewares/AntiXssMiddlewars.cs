﻿using Ganss.Xss;
using System.Web;

namespace TaskTracker.Middlewares
{
    public class AntiXssMiddleware
    {
        private readonly RequestDelegate _next;

        public AntiXssMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            // Check XSS in request content
            var originalBody = context.Request.Body;
            try
            {
                var route = context.Request.Path.ToString();

                if (!(context.Request.Method == "POST" && context.Request.ContentType != null && context.Request.ContentType.ToLower().Contains("multipart/form-data")))
                {
                    var content = await ReadRequestBody(context);
                    content = content == null ? "" : content.Trim();

                    var sanitizer = new HtmlSanitizer();
                    sanitizer.AllowedTags.Clear();

                    var sanitized = sanitizer.Sanitize(content);
                    sanitized = sanitized
                                    .Replace("\r", "")
                                    .Replace("&amp;", "&")
                                    .Replace("&quot;", "\"")
                                    .Replace("&lt;", "<")
                                    .Replace("&rsquo;", "'")
                                    .Replace("&apos;", "'")
                                    .Replace("&gt;", ">")
                                    .Replace("&#", "")
                                    .Replace("&nbsp;", " ")
                                    .Replace("&not", "¬")
                                    .Replace("&reg", "®");
                    content = content
                                    .Replace("\r", "")
                                    .Replace(" />", ">")
                                    .Replace("&amp;", "&")
                                    .Replace("&quot;", "\"")
                                    .Replace("&lt;", "<")
                                    .Replace("&gt;", ">")
                                    .Replace("&rsquo;", "'")
                                    .Replace("&apos;", "'")
                                    .Replace("&#", "")
                                    .Replace("&nbsp;", " ")
                                    .Replace("&not", "¬")
                                    .Replace("&reg", "®");

                    if (sanitized.Length != content.Length)
                    {
                        throw new BadHttpRequestException("Please check the fields. It contains potentially dangerous scripts.");
                    }
                }
                await _next(context).ConfigureAwait(false);
            }
            finally
            {
                context.Request.Body = originalBody;
            }
        }

        private static async Task<string> ReadRequestBody(HttpContext context)
        {
            // we can't dispose any item in the middleware as this middleware will pass the req to another middleware and disposed item will be unavailable.
            var buffer = new MemoryStream();
            await context.Request.Body.CopyToAsync(buffer);
            context.Request.Body = buffer;
            buffer.Position = 0;

            var reader = new StreamReader(buffer, true);
            var requestContent = await reader.ReadToEndAsync();
            requestContent = HttpUtility.UrlDecode(requestContent);
            context.Request.Body.Position = 0;

            return requestContent;
        }
    }

    public static class AntiXssMiddlewareExtension
    {
        public static IApplicationBuilder UseAntiXssMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AntiXssMiddleware>();
        }
    }
}
