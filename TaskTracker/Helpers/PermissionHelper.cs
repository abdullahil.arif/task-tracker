﻿using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Extensions;
using TaskTracker.Service.Services;

namespace TaskTracker.Helpers
{
    public class PermissionHelper : IPermissionHelper
    {
        #region Fields
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWorkspaceService _workspaceService;
        private readonly IWorkContext _workContext;
        #endregion

        #region Ctor
        public PermissionHelper(
            IHttpContextAccessor httpContextAccessor,
            IWorkspaceService workspaceService,
            IWorkContext workContext
            )
        {
            _httpContextAccessor = httpContextAccessor;
            _workspaceService = workspaceService;
            _workContext = workContext;
        }
        #endregion

        #region Methods
        public async Task<RolePermission> GetCurrentUserAccessRightsByWorkspace(int workspaceId)
        {
            var permission = _httpContextAccessor.HttpContext.Session.GetObject<RolePermission>("AccessRights");
            if (permission == null)
            {
                var workspace = await _workspaceService.GetWorkspaceDetailsAsync(workspaceId);
                permission = SetUserAccessRightsByWorkspace(workspace);
            }
            return permission;
        }

        public RolePermission SetUserAccessRightsByWorkspace(Workspace workspace)
        {
            var userId = _workContext.UserId;
            var roleId = workspace.WorkspaceMembers.Single(x => x.UserId == userId).RoleId;
            var permission = workspace.WorkspaceRoles.Single(x => x.Id == roleId).Permission;
            _httpContextAccessor.HttpContext.Session.SetObject("AccessRights", permission);
            return permission;
        }

        public void ResetWorkspaceAccess()
        {
            _httpContextAccessor.HttpContext.Session.Remove("WorkspaceList");
            _httpContextAccessor.HttpContext.Session.Remove("AccessRights");
        }

        public async Task<bool> CheckWorkspaceAccess(int workspaceId)
        {
            var accessibleWorkspaces = _httpContextAccessor.HttpContext.Session.GetObject<List<int>>("WorkspaceList");

            if (accessibleWorkspaces == null)
            {
                var workspaces = await _workspaceService.GetWorkspaceListAsync(_workContext.UserId);
                accessibleWorkspaces = workspaces.Select(x => x.Id).ToList();
                _httpContextAccessor.HttpContext.Session.SetObject("WorkspaceList", accessibleWorkspaces);
            }

            if (accessibleWorkspaces.Any(x => x == workspaceId) == false)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
