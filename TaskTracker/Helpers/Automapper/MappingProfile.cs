﻿using AutoMapper;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Domain.Workspaces;
using TaskTracker.Models;

namespace TaskTracker.Helpers.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Project
            CreateMap<Project, ProjectModel>();
            CreateMap<UpdateProjectModel, Project>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            #endregion

            #region Issue
            CreateMap<Issue, IssueModel>().ReverseMap();
            CreateMap<CreateIssueModel, Issue>();
            CreateMap<IssuePriority, IssuePriorityModel>().ReverseMap();
            CreateMap<IssueStatus, IssueStatusModel>().ReverseMap();
            #endregion

            #region WorkSpace
            CreateMap<Workspace, WorkspaceModel>();
            CreateMap<WorkspaceMember, WorkspaceMemberModel>();
            #endregion

            #region User & Permission
            CreateMap<AppUser, AppUserModel>();
            CreateMap<AppRole, AppRoleModel>();
            CreateMap<RolePermission, RolePermissionModel>();
            #endregion


            CreateMap<Comment, CommentModel>().ReverseMap();

            //CreateMap<IssueFilterParams, IssueFilterParamsModel>().ReverseMap();
            //CreateMap<IssueHistory, IssueHistoryModel>().ReverseMap();

            //CreateMap<Status, AttributeCustomizationModel>()
            //    .ForMember(dest => dest.Type, opt => opt.MapFrom(src => "S"));
            //CreateMap<Priority, AttributeCustomizationModel>()
            //      .ForMember(dest => dest.Type, opt => opt.MapFrom(src => "P"));
            //CreateMap<Priority, SelectListItem>()
            //    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id.ToString()))
            //    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Description));
            //CreateMap<Status, SelectListItem>()
            // .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id.ToString()))
            // .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Description));
        }
    }
}
