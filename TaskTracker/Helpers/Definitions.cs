﻿namespace TaskTracker.Helpers
{
    public static class SessionKeys
    {
        public const string UserId = "UserId";
        public const string UserName = "UserName";
    }
    public static class ErrorMessages
    {
        public const string UnAuthorized = "You are not authorized to perform this action.";
        public const string Generic = "An error occurred. Please try again.";
    }
}
