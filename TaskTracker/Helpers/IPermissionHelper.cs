﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Helpers
{
    public interface IPermissionHelper
    {
        Task<RolePermission> GetCurrentUserAccessRightsByWorkspace(int workspaceId);
        RolePermission SetUserAccessRightsByWorkspace(Workspace workspace);
        Task<bool> CheckWorkspaceAccess(int workspaceId);
        void ResetWorkspaceAccess();
    }
}