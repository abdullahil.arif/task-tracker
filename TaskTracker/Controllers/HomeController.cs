﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TaskTracker.Core;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;

namespace TaskTracker.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly IWorkContext _workContext;
        private readonly IPermissionHelper _permissionHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<HomeController> _logger;

        public HomeController(
            IProjectService projectService,
            IWorkContext workContext,
            IMapper mapper,
            ILogger<HomeController> logger,
            IPermissionHelper permissionHelper)
        {
            _projectService = projectService;
            _workContext = workContext;
            _mapper = mapper;
            _logger = logger;
            _permissionHelper = permissionHelper;
        }

        public async Task<IActionResult> Index()
        {
            var workspaceId = await _workContext.GetCurrentWorkspaceIdAsync();
            await InitAccessRights(workspaceId);
            var projects = await _projectService.GetProjectListByWorkspace(workspaceId);
            var model = new DashboardViewModel
            {
                Projects = _mapper.Map<List<ProjectModel>>(projects)
            };
            return View(model);
        }   
        public IActionResult Inbox()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Private Methods
        private async Task InitAccessRights(int workspaceId)
        {
            var accessRights = await _permissionHelper.GetCurrentUserAccessRightsByWorkspace(workspaceId);
            ViewBag.CreateProject = accessRights.CreateProject;
        }
        #endregion
    }
}