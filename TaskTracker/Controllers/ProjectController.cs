﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Extensions;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;

namespace TaskTracker.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;
        private readonly IWorkspaceService _workspaceService;
        private readonly IIssueService _issueService;
        private readonly IPermissionHelper _permissionHelper;
        private readonly IWorkContext _workContext;
        private readonly ILogger<ProjectController> _logger;
        #endregion

        #region Ctor
        public ProjectController(
            IMapper mapper,
            IProjectService projectService,
            IWorkContext workContext,
            ILogger<ProjectController> logger,
            IWorkspaceService workspaceService,
            IPermissionHelper permissionHelper,
        IIssueService issueService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _workContext = workContext;
            _logger = logger;
            _workspaceService = workspaceService;
            _permissionHelper = permissionHelper;
            _issueService = issueService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> Index(int id)
        {
            var project = await _projectService.GetProjectDetailsAsync(id);
            if (project == null)
            {
                return NotFound();
            }
            if (await _permissionHelper.CheckWorkspaceAccess(project.WorkspaceId) == false)
            {
                return Unauthorized();
            }
            var model = _mapper.Map<ProjectModel>(project);
            await InitAccessRights(model.WorkspaceId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string name, string description)
        {
            // todo: add workspaceid parameter
            try
            {
                var ret = await _projectService.CreateProjectAsync(new Project
                {
                    Name = name,
                    Description = description,
                    WorkspaceId = await _workContext.GetCurrentWorkspaceIdAsync(),
                    CreatedById = _workContext.UserId
                });
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(UpdateProjectModel model)
        {
            try
            {
                var project = await _projectService.GetProjectDetailsAsync(model.Id);
                if (project == null)
                {
                    return NotFound();
                }
                var newProject = _mapper.Map(model, project);
                var ret = await _projectService.UpdateProjectAsync(newProject);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _projectService.DeleteProjectAsync(id);
                return Json(new StatusResult { IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetStatus(int id)
        {
            var status = await _projectService.GetStatusAsync(id);
            return PartialView("_StatusCustomizationModal", _mapper.Map<IssueStatusModel>(status));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStatus(IssueStatusModel model)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(model.WorkspaceId) == false)
                {
                    return Unauthorized();
                }
                await InitAccessRights(model.WorkspaceId);
                var status = await _projectService.UpdateStatusAsync(_mapper.Map<IssueStatus>(model));
                var view = await this.RenderViewAsync("_StatusEntry", _mapper.Map<IssueStatusModel>(status.Data), true);
                return Json(new StatusResult<string> { IsSuccess = true, Data = view });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = "An error occurred. Please try again." });
            }

        }
        [HttpPost]
        public async Task<IActionResult> DeleteStatus(int id)
        {
            try
            {
                var ret = await _projectService.DeleteStatusAsync(id);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }
        #endregion

        #region Private Methods
        private async Task InitAccessRights(int workspaceId)
        {
            var accessRights = await _permissionHelper.GetCurrentUserAccessRightsByWorkspace(workspaceId);
            ViewBag.CreateTask = accessRights.CreateTask;
            ViewBag.DeleteTask = accessRights.DeleteTask;
            ViewBag.EditTask = accessRights.EditTask;
            ViewBag.EditProject = accessRights.EditProject;
        }
        #endregion
    }
}
