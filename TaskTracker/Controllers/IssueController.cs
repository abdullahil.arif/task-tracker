﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Extensions;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;

namespace TaskTracker.Controllers
{
    [Authorize]
    public class IssueController : Controller
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;
        private readonly IWorkspaceService _workspaceService;
        private readonly IIssueService _issueService;
        private readonly IPermissionHelper _permissionHelper;
        private readonly IWorkContext _workContext;
        private readonly ILogger<IssueController> _logger;
        #endregion

        #region Ctor
        public IssueController(
            IMapper mapper,
            IProjectService projectService,
            IWorkContext workContext,
            ILogger<IssueController> logger,
            IWorkspaceService workspaceService,
            IIssueService issueService,
            IPermissionHelper permissionHelper)
        {
            _mapper = mapper;
            _projectService = projectService;
            _workContext = workContext;
            _logger = logger;
            _workspaceService = workspaceService;
            _issueService = issueService;
            _permissionHelper = permissionHelper;
        }
        #endregion

        #region Methods
        [HttpGet]
        public async Task<IActionResult> CreateIssue([FromQuery] int projectId)
        {
            var vm = new CreateIssueViewModel
            {
                ProjectId = projectId,
                FieldValues = await GetIssueFieldValuesAsync(projectId)
            };
            return PartialView("_CreateIssueModal", vm);
        }

        [HttpPost]
        public async Task<IActionResult> Createissue(CreateIssueModel model)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(model.WorkspaceId) == false)
                {
                    return Unauthorized();
                }
                var issue = _mapper.Map<Issue>(model);
                issue.CreatedById = _workContext.UserId;
                var ret = await _issueService.CreateIssueAsync(issue);
                await InitAccessRights(model.WorkspaceId);
                return Json(new StatusResult<string>
                {
                    IsSuccess = true,
                    Data = await this.RenderViewAsync("_IssueEntry", _mapper.Map<IssueModel>(issue), true)
                });


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }

        }

        [HttpGet]
        public async Task<IActionResult> Preview(int workspaceId, int issueId)
        {

            if (await _permissionHelper.CheckWorkspaceAccess(workspaceId) == false)
            {
                return Unauthorized();
            }
            await InitAccessRights(workspaceId);

            var issue = await _issueService.GetIssueDetailsAsync(issueId);
            var model = new EditIssueViewModel
            {
                Issue = _mapper.Map<IssueModel>(issue),
                FieldValues = await GetIssueFieldValuesAsync(issue.ProjectId)
            };
            return PartialView("_IssuePreview", model);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int workspaceId, int issueId)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(workspaceId) == false)
                {
                    return Unauthorized();
                }
                await InitAccessRights(workspaceId);
                var issue = await _issueService.GetIssueDetailsAsync(issueId);
                var view = await this.RenderViewAsync("_IssueEntry", _mapper.Map<IssueModel>(issue), true);
                return Json(new StatusResult<string> { IsSuccess = true, Data = view });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var ret = await _issueService.DeleteIssueAsync(id);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProperty(int id, string fieldName, string value)
        {
            try
            {
                var ret = await _issueService.UpdatePropertyAsync(id, fieldName, value);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(int issueId, string text)
        {
            try
            {
                var ret = await _issueService.AddCommentAsync(issueId, text, _workContext.UserId);
                return Json(new StatusResult<string>
                {
                    IsSuccess = true,
                    Data = await this.RenderViewAsync("_CommentEntry", _mapper.Map<CommentModel>(ret.Data), true)
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdateComment(int commentId, string text)
        {
            try
            {
                var ret = await _issueService.UpdateCommentAsync(commentId, text);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = "An error occurred. Please try again." });
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteComment(int commentId)
        {
            try
            {
                var ret = await _issueService.DeleteCommentAsync(commentId);
                return Json(ret);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }
        #endregion



        #region Private Methods
        private async Task<IssueFieldValues> GetIssueFieldValuesAsync(int projectId)
        {
            var project = await _projectService.GetProjectDetailsAsync(projectId);
            var users = await _workspaceService.GetWorkspaceUserListAsync(project.WorkspaceId);
            return new IssueFieldValues
            {
                Priorities = _mapper.Map<List<IssuePriorityModel>>(project.Priorities),
                Statuses = _mapper.Map<List<IssueStatusModel>>(project.Statuses),
                Users = _mapper.Map<List<AppUserModel>>(users)
            };
        }

        private async Task InitAccessRights(int workspaceId)
        {
            var accessRights = await _permissionHelper.GetCurrentUserAccessRightsByWorkspace(workspaceId);
            ViewBag.DeleteTask = accessRights.DeleteTask;
            ViewBag.EditTask = accessRights.EditTask;
        }
        #endregion
    }
}
