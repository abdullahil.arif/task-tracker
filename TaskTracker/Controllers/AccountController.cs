﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core.Domain;
using TaskTracker.Models.Account;
using System.Security.Claims;
using System.Text.Json;
using TaskTracker.Service.Services;

namespace TaskTracker.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IWorkspaceService _workspaceService;
        private readonly ILogger<AccountController> _logger;
        #endregion

        #region Ctor
        public AccountController(
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager,
                IWorkspaceService workspaceService,
                ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _workspaceService = workspaceService;
            _logger = logger;
        }
        #endregion

        #region Methods
        [HttpGet]
        public IActionResult Login()
        {
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                    if (result.Succeeded)
                    {
                        await CookieSigninAsync(user, model.RememberMe);
                        return RedirectToAction("index", "home");
                    }
                    else
                    {
                        _logger.LogInformation("Invalid login attempt.", new { Email = model.Email });
                    }
                }
                ModelState.AddModelError(string.Empty, "Username or password is incorrect.");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new AppUser { UserName = model.Email, Name = model.Name, Email = model.Email, PictureUrl = "/images/placeholder-profile.jpg" };

                    var result = await _userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        await CookieSigninAsync(user, false);
                        await _workspaceService.CreateDefaultWorkspaceAsync(user);
                        return RedirectToAction("index", "home");
                    }

                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> IsEmailInUse(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return Json(true);
            }
            else
            {
                return Json($"Email {email} already in use");
            }
        }

        public async Task<IActionResult> LogOut()
        {
            HttpContext.Session?.Clear();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return LocalRedirect("/");
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePassword(PasswordUpdateModel model)
        {
            var user = await _userManager.FindByIdAsync(HttpContext.User?.Identity?.Name);

            if (user == null)
            {
                return Json("An error occurred.");
            }
            else
            {
                var passwordCheckResult = await _userManager.CheckPasswordAsync(user, model.OldPassword);
                if (passwordCheckResult)
                {
                    var passwordChangeResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (passwordChangeResult.Succeeded)
                    {
                        return Json(new { IsSuccess = true });
                    }
                }
                return Json(new { IsSuccess = false, ErrorMessage = "Failed to change password" });
            }
        }

        #endregion

        #region Private Methods
        private async Task CookieSigninAsync(AppUser user, bool isPersistent = false)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Email, user.Email),
            };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties()
            {
                IsPersistent = isPersistent
            });

            HttpContext.Session.SetString("currentUser", JsonSerializer.Serialize(user));
        }
        #endregion

    }
}
