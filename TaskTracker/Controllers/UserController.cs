﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;
using TaskTracker.Service.Services.User;

namespace TaskTracker.Controllers
{
    public class UserController : Controller
    {
        #region Fields
        private readonly UserManager<AppUser> _userManager;
        private readonly IPhotoService _photoService;
        private readonly IWorkContext _workContext;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _logger;
        #endregion

        #region Ctor
        public UserController(
            UserManager<AppUser> userManager,
            IPhotoService photoService,
            IWorkContext workContext,
            IMapper mapper,
            ILogger<UserController> logger)
        {
            _userManager = userManager;
            _mapper = mapper;
            _photoService = photoService;
            _workContext = workContext;
            _logger = logger;
        }
        #endregion

        #region Methods
        public async Task<IActionResult> SearchUsers(string email)
        {
            var users = await _userManager.FindByEmailAsync($"%{email}%");
            return Json(_mapper.Map<List<AppUserModel>>(users));
        }

        public async Task<IActionResult> Profile()
        {
            var user = await _userManager.FindByIdAsync(_workContext.UserId);
            var model = new UserProfileViewModel()
            {
                User = _mapper.Map<AppUserModel>(user)
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePicture(IFormFile file)
        {
            var ret = await _photoService.UploadPhotoToServerAsync(file);
            if (ret.IsSuccess)
            {
                var user = await _userManager.FindByIdAsync(_workContext.UserId.ToString());
                user.PictureUrl = Url.Content($"~/uploads/{ret.Data}");
                await _userManager.UpdateAsync(user);
                ret.Data = user.PictureUrl;
            }
            return Json(ret);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProfile(AppUserModel model)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(_workContext.UserId);
                user.Name = model.Name;
                user.Email = model.Email;
                await _userManager.UpdateAsync(user);
                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }
        #endregion
    }
}
