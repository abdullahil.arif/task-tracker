﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;

namespace TaskTracker.Controllers
{
    public class WorkspaceController : Controller
    {
        #region Fields
        private readonly IWorkspaceService _workspaceService;
        private readonly IWorkContext _workContext;
        private readonly IPermissionHelper _permissionHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<WorkspaceController> _logger;
        #endregion

        #region Ctor
        public WorkspaceController(
            IWorkspaceService workspaceService,
            IWorkContext workContext,
            IMapper mapper,
            ILogger<WorkspaceController> logger,
            IPermissionHelper permissionHelper)
        {
            _workspaceService = workspaceService;
            _workContext = workContext;
            _mapper = mapper;
            _logger = logger;
            _permissionHelper = permissionHelper;
        }
        #endregion

        #region Public Methods
        [HttpGet]
        public async Task<IActionResult> Index(int id)
        {
            if (await _permissionHelper.CheckWorkspaceAccess(id) == false)
            {
                return NotFound();
            }

            HttpContext?.Session.SetInt32("WorkspaceId", id);
            var workspace = await _workspaceService.GetWorkspaceDetailsAsync(id);
            await InitAccessRights(workspace.Id);

            var model = new WorkspaceViewModel()
            {
                Workspace = _mapper.Map<WorkspaceModel>(workspace)
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(string name, string description)
        {
            try
            {
                var ret = await _workspaceService.CreateWorkspaceAsync(_workContext.UserId, name, description);
                _permissionHelper.ResetWorkspaceAccess();
                HttpContext?.Session.SetInt32("WorkspaceId", ret.Id);
                return Json(new StatusResult<int> { IsSuccess = true, Data = ret.Id });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }

        }

        [HttpPost]
        public async Task<IActionResult> InviteUser(int workspaceId, string email)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(workspaceId) == false)
                {
                    return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.UnAuthorized });
                }

                var ret = await _workspaceService.InviteUserAsync(workspaceId, email);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }

        }

        [HttpPost]
        public async Task<IActionResult> RemoveUser(int workspaceId, string userId)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(workspaceId) == false)
                {
                    return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.UnAuthorized });
                }
                var ret = await _workspaceService.RemoveUserAsync(workspaceId, userId);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetRolePermission(int id)
        {
            var ret = await _workspaceService.GetRolePermissionAsync(id);
            return PartialView("_RolePermissionTable", _mapper.Map<RolePermissionModel>(ret));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserRole(int workspaceId, string userId, string roleId)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(workspaceId) == false)
                {
                    return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.UnAuthorized });
                }
                var ret = await _workspaceService.UpdateUserRoleAsync(workspaceId, userId, roleId);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateInfo(int id, string name = "", string description = "")
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(id) == false)
                {
                    return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.UnAuthorized });
                }
                var ret = await _workspaceService.UpdateWorkspaceDetailsAsync(id, name, description);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAssignedWorkspaces()
        {
            var workspaces = await _workspaceService.GetWorkspaceListAsync(_workContext.UserId);
            return PartialView("_SelectWorkspaces", _mapper.Map<List<WorkspaceModel>>(workspaces));
        }

        [HttpGet]
        public IActionResult SwitchWorkspace(int id)
        {
            _permissionHelper.ResetWorkspaceAccess();
            HttpContext?.Session.SetInt32("WorkspaceId", id);
            return Redirect("/");
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (await _permissionHelper.CheckWorkspaceAccess(id) == false)
                {
                    return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.UnAuthorized });
                }
                var ret = await _workspaceService.DeleteWorkspaceAsync(_workContext.UserId, id);
                return Json(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json(new StatusResult { IsSuccess = false, ErrorMessage = ErrorMessages.Generic });
            }
        }
        #endregion

        #region Private Methods
        private async Task InitAccessRights(int workspaceId)
        {
            var accessRights = await _permissionHelper.GetCurrentUserAccessRightsByWorkspace(workspaceId);
            ViewBag.RemoveUser = accessRights.RemoveWorkspaceUser;
            ViewBag.AddUser = accessRights.AddWorkspaceUser;
        }
        #endregion
    }
}
