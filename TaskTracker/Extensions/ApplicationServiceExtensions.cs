﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core;
using TaskTracker.Data;
using TaskTracker.Data.DbContext;
using TaskTracker.Data.Repositories;
using TaskTracker.Helpers;
using TaskTracker.Helpers.Automapper;
using TaskTracker.Service.Services;

namespace TaskTracker.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {

            // Register services and repositories
            services.AddScoped<IWorkContext, WebWorkContext>();

            //// Services
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IIssueService, IssueService>();
            services.AddScoped<IWorkspaceService, WorkspaceService>();
            services.AddScoped<IPhotoService, PhotoService>();
           
            
            services.AddScoped<IPermissionHelper, PermissionHelper>();

            // Repositories
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(EntityRepository<>));

            // AutoMapper
            services.AddAutoMapper(typeof(MappingProfile));


            // Session
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(120);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            var connectionString = config.GetConnectionString("DefaultConnection");

            services.AddDbContext<AppDbContext>(x => x.UseSqlServer(connectionString));

            return services;
        }

    }
}
