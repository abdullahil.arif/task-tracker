﻿namespace TaskTracker.Extensions
{
    public static class DateTimeExtension
    {
        public static string ToDayMonthNameShortString(this DateTime dateTime)
        {
            if(dateTime == null || dateTime == default(DateTime))
            {
                return string.Empty;
            }
            string monthName = dateTime.ToString("MMMM");
            return $"{dateTime.Day.ToString().PadLeft(2, '0')} {monthName.Substring(0, 3)} {dateTime.Year}";
        }

        public static string ToRecentTimeString(this DateTime dateTime)
        {
            TimeSpan timeDifference = DateTime.Now - dateTime;

            if (timeDifference.TotalMinutes < 1)
            {
                return "Just now";
            }
            else if (timeDifference.TotalHours < 1)
            {
                int minutesAgo = (int)timeDifference.TotalMinutes;
                return $"{minutesAgo} minute{(minutesAgo != 1 ? "s" : "")} ago";
            }
            else if (timeDifference.TotalHours < 24)
            {
                int hoursAgo = (int)timeDifference.TotalHours;
                return $"{hoursAgo} hour{(hoursAgo != 1 ? "s" : "")} ago";
            }
            else if (timeDifference.TotalDays < 2)
            {
                return "Yesterday";
            }
            else
            {
                string monthName = dateTime.ToString("MMMM");
                return $"{dateTime.Day.ToString().PadLeft(2, '0')} {monthName}, {dateTime.Year}";
            }
        }
    }
}
