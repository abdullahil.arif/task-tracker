﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaskTracker.Core;
using TaskTracker.Core.Domain;
using TaskTracker.Helpers;
using TaskTracker.Models;
using TaskTracker.Service.Services;

namespace TaskTracker.Components
{
    public class NavbarViewComponent : ViewComponent
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IWorkspaceService _workspaceService;
        private readonly IProjectService _projectService;
        private readonly IWorkContext _workContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPermissionHelper _permissionHelper;
        #endregion

        #region Ctor
        public NavbarViewComponent(
            IMapper mapper,
            IWorkspaceService workspaceService,
            IProjectService projectService,
            IWorkContext workContext,
            IHttpContextAccessor httpContextAccessor,
            IPermissionHelper permissionHelper)
        {
            _mapper = mapper;
            _workspaceService = workspaceService;
            _projectService = projectService;
            _workContext = workContext;
            _httpContextAccessor = httpContextAccessor;
            _permissionHelper = permissionHelper;
        }
        #endregion

        #region Public Methods
        public async Task<IViewComponentResult> InvokeAsync()
        {

            var user = await _workContext.GetCurrentUserAsync();
            if (user == null)
            {
                _httpContextAccessor.HttpContext.Response.Redirect("/Account/login");
                return View(new BaseViewModel());
            }
            var workspaceId = await _workContext.GetCurrentWorkspaceIdAsync();
            var workspace = await _workspaceService.GetWorkspaceDetailsAsync(workspaceId);

            var projects = await _projectService.GetProjectListByWorkspace(workspaceId);
            var model = new BaseViewModel
            {
                User = _mapper.Map<AppUserModel>(user),
                Workspace = _mapper.Map<WorkspaceModel>(workspace),
                Projects = _mapper.Map<List<ProjectModel>>(projects),
                Permissions = SetUserAccessRights(workspace)
            };

            return View(model);
        }


        #endregion

        #region Private Methods
        private RolePermissionModel SetUserAccessRights(Workspace workspace)
        {
            var permission = _permissionHelper.SetUserAccessRightsByWorkspace(workspace);
            var model = _mapper.Map<RolePermissionModel>(permission);
            return model;
        }
        #endregion
    }
}
