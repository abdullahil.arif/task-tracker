﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Service
{
    public static class DefaultRoles
    {
        public const string Admin = "Admin";
        public const string Member = "Member";
        public const string Guest = "LimitedMember";
    }
}
