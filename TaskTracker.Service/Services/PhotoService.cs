﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using TaskTracker.Core.Domain;

namespace TaskTracker.Service.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly ILogger<PhotoService> _logger;

        public PhotoService(ILogger<PhotoService> logger)
        {
            _logger = logger;
        }
        public async Task<StatusResult<string>> UploadPhotoToServerAsync(IFormFile file)
        {
            try
            {
                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png" };
                var fileExtension = Path.GetExtension(file.FileName).ToLowerInvariant();

                if (!allowedExtensions.Contains(fileExtension))
                {
                    return new StatusResult<string> { IsSuccess = false, ErrorMessage = "Unsupported type." };
                }

                var fileName = $"Profile_{DateTime.Now.Ticks}{fileExtension}";
                var filePath = Path.Combine("wwwroot", "uploads", fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                return new StatusResult<string> { IsSuccess = true, Data = fileName };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to upload photo.");
            }
            return new StatusResult<string> { IsSuccess = false, ErrorMessage = "Failed to upload photo." };

        }
    }
}
