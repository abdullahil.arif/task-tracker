﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.Design;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Specification;
using TaskTracker.Data;

namespace TaskTracker.Service.Services
{
    public class IssueService : IIssueService
    {
        #region Fields
        private readonly UserManager<AppUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public IssueService(IUnitOfWork unitOfWork, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
        #endregion

        #region Public Methods
        public async Task<Issue> CreateIssueAsync(Issue issue)
        {
            _unitOfWork.IssueRepository.Insert(issue);
            await _unitOfWork.SaveChangesAsync();
            return await GetIssueDetailsAsync(issue.Id);
        }

        public async Task<Issue> GetIssueDetailsAsync(int id)
        {
            var spec = new Specification<Issue>(x => x.Id == id);
            spec.AddInclude(
                q => q.Include(q => q.Status)
                        .Include(q => q.Priority)
                        .Include(q => q.Assignee)
                        .Include(q => q.Comments)
                        .ThenInclude(q => q.CreatedBy)

                 );
            var data = await _unitOfWork.IssueRepository.GetAsync(spec);
            return data;
        }

        public async Task<StatusResult> DeleteIssueAsync(int id)
        {
            var issue = new Issue { Id = id };
            _unitOfWork.IssueRepository.Delete(issue);
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }

        public async Task<StatusResult> UpdatePropertyAsync(int id, string fieldName, string value)
        {
            var issue = new Issue { Id = id };
            if (string.Equals(fieldName, "title", StringComparison.OrdinalIgnoreCase))
            {
                issue.Title = value;
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.Title);
            }
            else if (string.Equals(fieldName, "description", StringComparison.OrdinalIgnoreCase))
            {
                issue.Description = value;
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.Description);
            }
            else if (string.Equals(fieldName, "status", StringComparison.OrdinalIgnoreCase))
            {
                issue.StatusId = Convert.ToInt32(value);
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.StatusId);
            }
            else if (string.Equals(fieldName, "priority", StringComparison.OrdinalIgnoreCase))
            {
                issue.PriorityId = Convert.ToInt32(value);
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.PriorityId);
            }
            else if (string.Equals(fieldName, "assignee", StringComparison.OrdinalIgnoreCase))
            {
                issue.AssigneeId = value;
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.AssigneeId);
            }
            else if (string.Equals(fieldName, "due date", StringComparison.OrdinalIgnoreCase))
            {
                issue.DueDate = DateTime.Parse(value);
                _unitOfWork.IssueRepository.UpdateProperty(issue, x => x.DueDate);
            }

            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };

            // todo: update issue history

        }

        public async Task<StatusResult<Comment>> AddCommentAsync(int issueId, string text, string userId)
        {
            var comment = new Comment
            {
                IssueId = issueId,
                CommentText = text,
                CreatedBy = await _userManager.FindByIdAsync(userId),
                CreatedOn = DateTime.UtcNow
            };
            _unitOfWork.GetRepository<Comment>().Insert(comment);
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult<Comment> { IsSuccess = true, Data = comment };
        }
        public async Task<StatusResult> UpdateCommentAsync(int commentId, string text)
        {
            var comment = new Comment { Id = commentId, CommentText = text };
            _unitOfWork.GetRepository<Comment>().UpdateProperty(comment, x => x.CommentText);
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }

        public async Task<StatusResult> DeleteCommentAsync(int commentId)
        {
            _unitOfWork.GetRepository<Comment>().Delete(new Comment { Id = commentId });
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }
        #endregion

        #region Private Methods
        #endregion
    }
}
