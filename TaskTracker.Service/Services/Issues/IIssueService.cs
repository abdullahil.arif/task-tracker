﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Service.Services
{
    public interface IIssueService
    {
        Task<Issue> CreateIssueAsync(Issue issue);
        Task<Issue> GetIssueDetailsAsync(int id);
        Task<StatusResult> DeleteIssueAsync(int id);
        Task<StatusResult> UpdatePropertyAsync(int id, string fieldName, string value);
        Task<StatusResult<Comment>> AddCommentAsync(int issueId, string text, string userId);
        Task<StatusResult> UpdateCommentAsync(int commentId, string text);
        Task<StatusResult> DeleteCommentAsync(int commentId);
    }
}