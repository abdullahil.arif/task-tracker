﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Domain.Workspaces;
using TaskTracker.Core.Specification;
using TaskTracker.Data;

namespace TaskTracker.Service.Services
{
    public class WorkspaceService : IWorkspaceService
    {
        #region Fields
        private readonly UserManager<AppUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public WorkspaceService(IUnitOfWork unitOfWork, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }
        #endregion

        #region Public Methods
        public async Task<Workspace> CreateWorkspaceAsync(string userId, string name, string description)
        {
            var data = new Workspace()
            {
                Name = name,
                Description = "",
                CreatedById = userId
            };

            var adminRole = CreateRoleWithDefaultPermission(DefaultRoles.Admin);
            var memberRole = CreateRoleWithDefaultPermission(DefaultRoles.Member);
            var guestRole = CreateRoleWithDefaultPermission(DefaultRoles.Guest);

            data.WorkspaceRoles = new List<AppRole>() { adminRole, memberRole, guestRole };

            data.WorkspaceMembers = new List<WorkspaceMember>()
            {
                new WorkspaceMember()
                {
                    UserId = userId,
                    Title = "Workspace Owner",
                    Role = adminRole
                }
            };

            _unitOfWork.WorkspaceRepository.Insert(data);
            await _unitOfWork.SaveChangesAsync();
            return data;
        }

        public async Task<Workspace> CreateDefaultWorkspaceAsync(AppUser user)
        {
            var userInitial = user.Name.Split(" ").FirstOrDefault();
            var workspaceName = $"{userInitial}'s Space";
            return await CreateWorkspaceAsync(user.Id, workspaceName, workspaceName);
        }
        public async Task<IEnumerable<Workspace>> GetWorkspaceListAsync(string userId)
        {
            var spec = new Specification<WorkspaceMember>(x => x.UserId == userId && x.Workspace.IsDeleted == false);
            spec.AddInclude(q => q.Include(q => q.Workspace).ThenInclude(q => q.CreatedBy));
            var data = await _unitOfWork.GetRepository<WorkspaceMember>().ListAsync(spec);
            return data.Select(x => x.Workspace).ToList();
        }

        public async Task<Workspace> GetDefaultWorkspaceAsync(string userId)
        {
            var data = await GetWorkspaceListAsync(userId);
            return data.FirstOrDefault();
        }

        public async Task<Workspace> GetWorkspaceDetailsAsync(int workspaceId)
        {
            var spec = new Specification<Workspace>(x => x.Id == workspaceId && x.IsDeleted == false);
            spec.AddInclude(
                q => q.Include(q => q.WorkspaceRoles)
                        .ThenInclude(q => q.Permission)
                        .Include(q => q.WorkspaceMembers)
                        .ThenInclude(q => q.User)
                 );
            var data = await _unitOfWork.WorkspaceRepository.GetAsync(spec);
            return data;
        }

        public async Task<StatusResult> InviteUserAsync(int workspaceId, string email)
        {
            // todo: send invitation
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var workspace = await GetWorkspaceDetailsAsync(workspaceId);
                if (workspace.WorkspaceMembers?.Any(x => x.UserId == user.Id) == false)
                {
                    workspace.WorkspaceMembers.Add(new WorkspaceMember()
                    {
                        UserId = user.Id,
                        Title = "Member",
                        Role = workspace.WorkspaceRoles.SingleOrDefault(x => x.Name == DefaultRoles.Member)
                    });

                    _unitOfWork.WorkspaceRepository.Update(workspace);
                    await _unitOfWork.SaveChangesAsync();
                }
            }

            return new StatusResult { IsSuccess = true };
        }

        public async Task<RolePermission> GetRolePermissionAsync(int permissionId)
        {
            var data = await _unitOfWork.GetRepository<RolePermission>().GetByIdAsync(permissionId);
            return data;
        }
        public async Task<StatusResult> UpdateUserRoleAsync(int workspaceId, string userId, string roleId)
        {
            var data = await GetWorkspaceDetailsAsync(workspaceId);
            if (data != null)
            {
                foreach (var member in data.WorkspaceMembers)
                {
                    if (member.UserId == userId)
                    {
                        member.RoleId = roleId;
                        break;
                    }
                }
                _unitOfWork.WorkspaceRepository.Update(data);
                await _unitOfWork.SaveChangesAsync();
            }
            return new StatusResult { IsSuccess = true };
        }
        public async Task<StatusResult> RemoveUserAsync(int workspaceId, string userId)
        {

            var workspace = await GetWorkspaceDetailsAsync(workspaceId);
            if (workspace.WorkspaceMembers?.Any(x => x.User.Id == userId) == true)
            {
                var member = workspace.WorkspaceMembers.SingleOrDefault(x => x.UserId == userId);
                _unitOfWork.GetRepository<WorkspaceMember>().Delete(member);
                await _unitOfWork.ExecuteRawSqlAsync(@"UPDATE [Issues] SET AssigneeId = NULL WHERE AssigneeId = @Id", new SqlParameter("Id", userId));
                await _unitOfWork.SaveChangesAsync();
            }

            return new StatusResult { IsSuccess = true };
        }
        public async Task<StatusResult> UpdateWorkspaceDetailsAsync(int workspaceId, string name, string description)
        {
            var workspace = new Workspace { Id = workspaceId, Name = name, Description = description };
            _unitOfWork.WorkspaceRepository.UpdateProperty(workspace, x => x.Name);
            _unitOfWork.WorkspaceRepository.UpdateProperty(workspace, x => x.Description);
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }

        public async Task<IEnumerable<AppUser>> GetWorkspaceUserListAsync(int id)
        {
            var workspace = await GetWorkspaceDetailsAsync(id);
            return workspace.WorkspaceMembers.Select(w => w.User).ToList();
        }

        public RolePermission GetUserAccessRights(string userId, Workspace workspace)
        {
            var roleId = workspace.WorkspaceMembers.Single(x => x.UserId == userId).RoleId;
            var permission = workspace.WorkspaceRoles.Single(x => x.Id == roleId).Permission;
            return permission;
        }

        public async Task<StatusResult> DeleteWorkspaceAsync(string userId, int id)
        {
            var spec = new Specification<Workspace>(x => x.CreatedById == userId && x.IsDeleted == false);
            var workspacesList = await _unitOfWork.WorkspaceRepository.ListAsync(spec);
            if(workspacesList.Count() == 1)
            {
                return new StatusResult { IsSuccess = false, ErrorMessage = "Can't delete only workspace." };
            }

            if(workspacesList.Any(x => x.Id == id))
            {
                var workspace = new Workspace { Id = id, IsDeleted = true };
                _unitOfWork.WorkspaceRepository.UpdateProperty(workspace, x => x.IsDeleted);
                await _unitOfWork.SaveChangesAsync();
                return new StatusResult { IsSuccess = true };
            }
            else
            {
                return new StatusResult { IsSuccess = false, ErrorMessage = "An error occurred. Please try again" };
            }
       
        }

        #endregion

        #region Private Methods
        private AppRole CreateRoleWithDefaultPermission(string roleName)
        {
            return new AppRole()
            {
                Name = roleName,
                Permission = GetDefaultPermissionByRole(roleName)
            };
        }
        private RolePermission GetDefaultPermissionByRole(string roleName)
        {
            switch (roleName)
            {
                case DefaultRoles.Admin:
                    return new RolePermission
                    {
                        CreateTask = true,
                        EditTask = true,
                        DeleteTask = true,
                        CreateProject = true,
                        EditProject = true,
                        DeleteProject = true,
                        AddWorkspaceUser = true,
                        RemoveWorkspaceUser = true
                    };
                case DefaultRoles.Member:
                    return new RolePermission
                    {
                        CreateTask = true,
                        EditTask = true,
                        DeleteTask = true
                    };
                default: return new RolePermission { };
            }
        }
        #endregion
    }
}