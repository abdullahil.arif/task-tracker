﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Service.Services
{
    public interface IWorkspaceService
    {
        Task<Workspace> CreateWorkspaceAsync(string userId, string name, string description);
        Task<Workspace> CreateDefaultWorkspaceAsync(AppUser user);
        Task<IEnumerable<Workspace>> GetWorkspaceListAsync(string userId);
        Task<Workspace> GetDefaultWorkspaceAsync(string userId);
        Task<Workspace> GetWorkspaceDetailsAsync(int workspaceId);
        Task<StatusResult> InviteUserAsync(int workspaceId, string email);
        Task<RolePermission> GetRolePermissionAsync(int permissionId);
        Task<StatusResult> UpdateUserRoleAsync(int workspaceId, string userId, string roleId);
        Task<StatusResult> RemoveUserAsync(int workspaceId, string userId);
        Task<StatusResult> UpdateWorkspaceDetailsAsync(int workspaceId, string name, string description);
        Task<IEnumerable<AppUser>> GetWorkspaceUserListAsync(int id);
        RolePermission GetUserAccessRights(string userId, Workspace workspace);
        Task<StatusResult> DeleteWorkspaceAsync(string userId, int id);
    }
}