﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using TaskTracker.Core;
using TaskTracker.Core.Domain;

namespace TaskTracker.Service.Services
{
    public interface IPhotoService
    {
        Task<StatusResult<string>> UploadPhotoToServerAsync(IFormFile file);
    }
}
