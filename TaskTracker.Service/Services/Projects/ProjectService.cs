﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Specification;
using TaskTracker.Data;

namespace TaskTracker.Service.Services
{
    public class ProjectService : IProjectService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        public async Task<StatusResult<int>> CreateProjectAsync(Project project)
        {
            _unitOfWork.ProjectRepository.Insert(PrepareProjectScaffoldData(project));
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult<int>
            {
                IsSuccess = true,
                Data = project.Id
            };
        }
        public async Task<StatusResult> UpdateProjectAsync(Project project)
        {
            _unitOfWork.ProjectRepository.Update(project);
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }
        public async Task<IEnumerable<Project>> GetProjectListByWorkspace(int workspaceId)
        {
            // todo: specification for project with only name vs all info
            var spec = new Specification<Project>(p => p.WorkspaceId == workspaceId && p.IsDeleted == false);
            var data = await _unitOfWork.ProjectRepository.ListAsync(spec);
            return data;
        }
        public async Task<Project> GetProjectDetailsAsync(int projectId)
        {
            var spec = new Specification<Project>(p => p.Id == projectId);
            spec.AddInclude(q => q.Include(x => x.Statuses).Include(x => x.Priorities).Include(x => x.Issues).ThenInclude(q => q.Assignee));
            var data = await _unitOfWork.ProjectRepository.GetAsync(spec);
            return data;
        }
        public async Task DeleteProjectAsync(int projectId)
        {
            var project = new Project { Id = projectId, IsDeleted = true };
            _unitOfWork.ProjectRepository.UpdateProperty(project, x => x.IsDeleted);
            await _unitOfWork.SaveChangesAsync();
        }
        public async Task<IssueStatus> GetStatusAsync(int id)
        {
            if (id == 0) return new IssueStatus() {  ColorCode = "primary" };
            else
            {
                var spec = new Specification<IssueStatus>(x => x.Id == id);
                return await _unitOfWork.StatusRepository.GetAsync(spec);
            }
        }

        public async Task<StatusResult<IssueStatus>> UpdateStatusAsync(IssueStatus status)
        {
            if (status.Id == 0)
            {
                _unitOfWork.StatusRepository.Insert(status);
            }
            else
            {
                _unitOfWork.StatusRepository.Update(status);
            }
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult<IssueStatus> {  IsSuccess = true, Data = status};
        }

        public async Task<StatusResult> DeleteStatusAsync(int id)
        {
            // todo: prevent deleting last one
            var issuesWithStatus = await _unitOfWork.IssueRepository.CountAsync(new Specification<Issue>(x => x.StatusId == id));
            if(issuesWithStatus > 0)
            {
                return new StatusResult { IsSuccess = false, ErrorMessage = "Can't Delete. There are tasks with this status." };
            }

            _unitOfWork.StatusRepository.Delete(new IssueStatus { Id = id });
            await _unitOfWork.SaveChangesAsync();
            return new StatusResult { IsSuccess = true };
        }

        #endregion

        #region Private Methods
        private Project PrepareProjectScaffoldData(Project project)
        {
            project.ColorCode = "primary-subtle";
            project.Icon = "fa fa-list";

            project.Statuses = new List<IssueStatus>()
            {
                new IssueStatus { Description = "Not Started", ColorCode = "secondary" },
                new IssueStatus { Description = "In Progress", ColorCode = "primary" },
                new IssueStatus { Description = "In Review", ColorCode = "warning" },
                new IssueStatus { Description = "Completed", ColorCode = "success" },
            };

            project.Priorities = new List<IssuePriority>()
            {
                new IssuePriority { Description = "Low", ColorCode = "success" },
                new IssuePriority { Description = "Medium", ColorCode = "warning" },
                new IssuePriority { Description = "Critical", ColorCode = "danger" }
            };

            return project;
        }
        #endregion
    }
}
