﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Service.Services
{
    public interface IProjectService
    {
        Task<StatusResult<int>> CreateProjectAsync(Project project);
        Task<IEnumerable<Project>> GetProjectListByWorkspace(int workspaceId);
        Task<Project> GetProjectDetailsAsync(int projectId);
        Task<StatusResult> UpdateProjectAsync(Project project);
        Task DeleteProjectAsync(int projectId);
        Task<IssueStatus> GetStatusAsync(int id);
        Task<StatusResult<IssueStatus>> UpdateStatusAsync(IssueStatus status);
        Task<StatusResult> DeleteStatusAsync(int id);
    }
}