﻿using TaskTracker.Data;

namespace TaskTracker.Service.Services.User
{
    public class UserService : IUserService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods

        #endregion
    }

    public interface IUserService
    {

    }
}
