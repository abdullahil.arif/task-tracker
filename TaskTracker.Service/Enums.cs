﻿namespace TaskTracker.Service
{
    public enum ProjectStatus
    {
        Running,
        Deleted,
        Archived,

    }
}