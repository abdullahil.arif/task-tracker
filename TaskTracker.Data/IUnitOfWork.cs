﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Domain;
using TaskTracker.Data.Repositories;

namespace TaskTracker.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Issue> IssueRepository { get; }
        IRepository<IssueStatus> StatusRepository { get; }
        IRepository<Workspace> WorkspaceRepository { get; }
        Task<int> SaveChangesAsync();
        Task<int> ExecuteRawSqlAsync(string sql, params object[] parameters);
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;
    }
}
