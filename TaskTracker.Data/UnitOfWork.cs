﻿using Microsoft.EntityFrameworkCore;
using TaskTracker.Core.Domain;
using TaskTracker.Data.DbContext;
using TaskTracker.Data.Repositories;

namespace TaskTracker.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly  AppDbContext _context;
        public IRepository<Project> ProjectRepository { get; private set; }
        public IRepository<Issue> IssueRepository { get; private set; }
        public IRepository<IssueStatus> StatusRepository { get; private set; }
        public IRepository<Workspace> WorkspaceRepository { get; private set; }

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            ProjectRepository = new EntityRepository<Project>(_context);
            IssueRepository = new EntityRepository<Issue>(_context);
            StatusRepository = new EntityRepository<IssueStatus>(_context);
            WorkspaceRepository = new EntityRepository<Workspace>(_context);
        }
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return new EntityRepository<TEntity>(_context);
        }

        public async Task<int> ExecuteRawSqlAsync(string sql, params object[] parameters)
        {
            return await _context.Database.ExecuteSqlRawAsync(sql, parameters);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
