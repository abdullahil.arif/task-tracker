﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Specification;
using TaskTracker.Data.DbContext;

namespace TaskTracker.Data.Repositories
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        #region Fields
        public readonly AppDbContext _context;
        #endregion 

        #region Ctor
        public EntityRepository(AppDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Get

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
        }   
        public async Task<TEntity> GetAsync(ISpecification<TEntity> spec)
        {
            return await ApplySpecification(spec).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<TEntity>> ListAsync(ISpecification<TEntity> spec)
        {
            return await ApplySpecification(spec).ToListAsync();
        }
        public async Task<int> CountAsync(ISpecification<TEntity> spec)
        {
            return await ApplySpecification(spec).CountAsync();
        }
        #endregion

        #region Insert
        public void Insert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Insert(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                _context.Set<TEntity>().Add(entity);
            }
        }
        #endregion

        #region Update
        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
        public void Update(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                _context.Set<TEntity>().Attach(entity);
                _context.Entry(entity).State = EntityState.Modified;
            }
        }
        public void UpdateProperty(TEntity entity, Expression<Func<TEntity, object>> property)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).Property(property).IsModified = true;
        }
        #endregion

        #region Delete
        public void Delete(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Set<TEntity>().Remove(entity);
        }
        public void Delete(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                _context.Set<TEntity>().Attach(entity);
                _context.Set<TEntity>().Remove(entity);
            }
        }
        #endregion

        #region Private Methods
        private IQueryable<TEntity> ApplySpecification(ISpecification<TEntity> spec)
        {
            return SpecificationEvaluator<TEntity>.GetQuery(_context.Set<TEntity>().AsQueryable(), spec);
        }
        #endregion
    }
}
