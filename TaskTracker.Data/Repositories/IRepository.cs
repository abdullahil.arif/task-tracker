﻿using System.Linq.Expressions;
using TaskTracker.Core.Domain;
using TaskTracker.Core.Specification;

namespace TaskTracker.Data.Repositories
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<TEntity> GetByIdAsync(int id);
        Task<TEntity> GetAsync(ISpecification<TEntity> spec);
        Task<IEnumerable<TEntity>> ListAsync(ISpecification<TEntity> spec);
        Task<int> CountAsync(ISpecification<TEntity> spec);
        void Insert(TEntity entity);
        void Insert(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void Update(IEnumerable<TEntity> entities);
        void UpdateProperty(TEntity entity, Expression<Func<TEntity, object>> property);
        void Delete(TEntity entity);
        void Delete(IEnumerable<TEntity> entities);
    }
}