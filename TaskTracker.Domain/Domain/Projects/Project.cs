﻿namespace TaskTracker.Core.Domain
{
    public class Project : BaseEntity
    {

        public string Name { get; set; }
        public string? Description { get; set; }
        public int CurrentStatus { get; set; } // Active, Completed, Archived
        public string CreatedById { get; set; }
        public int WorkspaceId { get; set; }
        public bool IsDeleted { get; set; }
        public string? ColorCode { get; set; }
        public string? Icon { get; set; }

        public virtual ICollection<Issue> Issues { get; set; }
        public virtual ICollection<IssueStatus> Statuses { get; set; }
        public virtual ICollection<IssuePriority> Priorities { get; set; }
    }


}
