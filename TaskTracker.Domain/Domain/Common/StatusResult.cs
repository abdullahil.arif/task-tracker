﻿namespace TaskTracker.Core.Domain
{
    public class StatusResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class StatusResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }

}
