﻿namespace TaskTracker.Core.Domain
{
    public class RolePermission : BaseEntity
    {
        public bool CreateTask { get; set; }
        public bool EditTask { get; set; }
        public bool DeleteTask { get; set; }
        public bool CreateProject { get; set; }
        public bool EditProject { get; set; }
        public bool DeleteProject { get; set; }
        public bool AddWorkspaceUser { get; set; }
        public bool RemoveWorkspaceUser { get; set; }
    }
}
