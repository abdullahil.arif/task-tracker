﻿using Microsoft.AspNetCore.Identity;

namespace TaskTracker.Core.Domain
{
    public class AppRole: IdentityRole 
    { 
        public virtual int WorkspaceId { get; set; }
        public virtual RolePermission Permission { get; set; }
    }
}
