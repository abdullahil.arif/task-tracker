﻿using Microsoft.AspNetCore.Identity;
using TaskTracker.Core.Domain.Workspaces;

namespace TaskTracker.Core.Domain
{
    public class AppUser : IdentityUser
    {
        public string Name { get; set; }
        public string PictureUrl { get; set; }
        public virtual ICollection<WorkspaceMember> WorkspaceMembers { get; set; }

    }
}
