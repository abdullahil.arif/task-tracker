﻿using TaskTracker.Core.Domain.Workspaces;

namespace TaskTracker.Core.Domain
{
    public class Workspace : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedById { get; set; }
        public bool IsDeleted { get; set; }
        public AppUser CreatedBy { get; set; }
        public virtual ICollection<WorkspaceMember> WorkspaceMembers { get; set; }
        public virtual ICollection<AppRole> WorkspaceRoles { get; set; }
    }
}
