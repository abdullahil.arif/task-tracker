﻿namespace TaskTracker.Core.Domain.Workspaces
{
    public class WorkspaceMember : BaseEntity
    {
        public int WorkspaceId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public virtual Workspace Workspace { get; set; }
        public virtual AppUser User { get; set; }
        public virtual AppRole Role { get; set; }
        public virtual string RoleId { get; set; }
    }
}
