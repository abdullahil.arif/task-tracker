﻿namespace TaskTracker.Core.Domain
{
    public class IssuePriority : BaseEntity
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
        public bool IsDefault { get; set; }
    }


}
