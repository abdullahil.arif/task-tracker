﻿namespace TaskTracker.Core.Domain
{
    public class Issue : BaseEntity
    {
        public int ProjectId { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public int? IssueType { get; set; }
        public int? PriorityId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public string? AssigneeId { get; set; }
        public string CreatedById { get; set; }
        public AppUser Assignee { get; set; }
        public AppUser CreatedBy { get; set; }
        public IssuePriority Priority { get; set; }
        public IssueStatus Status { get; set; }
        public List<SubTask> SubTasks { get; set; }
        public List<Comment> Comments { get; set; }
    }


}
