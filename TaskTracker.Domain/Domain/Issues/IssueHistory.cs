﻿namespace TaskTracker.Core.Domain
{
    public class IssueHistory : BaseEntity
    {
        public int IssueId { get; set; }
        public string Description { get; set; }
        public virtual AppUser CreatedBy { get; set; }
    }


}
