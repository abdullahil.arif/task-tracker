﻿namespace TaskTracker.Core.Domain
{
    public class IssueFilterParams
    {
        public int Type { get; set; } = -1;
        public int Status { get; set; } = -1;
        public int Priority { get; set; } = -1;
        public int Assignee { get; set; } = -1;
    }


}
