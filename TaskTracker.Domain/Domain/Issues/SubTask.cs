﻿namespace TaskTracker.Core.Domain
{
    public class SubTask : BaseEntity
    {
        public int IssueId { get; set; }
        public string Description { get; set; }
        public bool IsCompleted { get; set; }
    }


}
