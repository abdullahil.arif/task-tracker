﻿namespace TaskTracker.Core.Domain
{
    public class Comment : BaseEntity
    {
        public int IssueId { get; set; }
        public string CommentText { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string CreatedById { get; set; }
        public virtual AppUser CreatedBy { get; set; }
    }


}
