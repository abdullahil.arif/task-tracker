﻿namespace TaskTracker.Core.Domain
{
    public class IssueStatus : BaseEntity
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public string ColorCode { get; set; }
    }


}
