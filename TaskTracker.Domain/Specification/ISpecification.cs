﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;
using TaskTracker.Core.Domain;

namespace TaskTracker.Core.Specification
{
    public interface ISpecification<TEntity> where TEntity : BaseEntity
    {
        List<Expression<Func<TEntity, bool>>> Criterias { get; }
        List<Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>> Includes { get; }
        Expression<Func<TEntity, object>> OrderBy { get; }
        Expression<Func<TEntity, object>> OrderByDescending { get; }
        int Take { get; }
        int Skip { get; }
        bool IsPagingEnabled { get; }
    }
}