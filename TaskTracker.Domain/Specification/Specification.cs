﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;
using TaskTracker.Core.Domain;

namespace TaskTracker.Core.Specification
{
    public class Specification<TEntity> : ISpecification<TEntity> where TEntity : BaseEntity
    {
        public Specification(Expression<Func<TEntity, bool>> criteria)
        {
            Criterias = new() { criteria };
            Includes = new();
        }

        public List<Expression<Func<TEntity, bool>>> Criterias { get; }
        public List<Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>> Includes { get; }
        public Expression<Func<TEntity, object>> OrderBy { get; private set; }
        public Expression<Func<TEntity, object>> OrderByDescending { get; private set; }
        public int Take { get; private set; }
        public int Skip { get; private set; }
        public bool IsPagingEnabled { get; private set; }
        public void AddCriteria(Expression<Func<TEntity, bool>> criteriaExpression)
        {
            Criterias.Add(criteriaExpression);
        }
        public void AddInclude(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeExpression)
        {
            Includes.Add(includeExpression);
        }
        public void AddOrderBy(Expression<Func<TEntity, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
        }
        public void AddOrderByDescending(Expression<Func<TEntity, object>> orderByDescExpression)
        {
            OrderByDescending = orderByDescExpression;
        }
        public void ApplyPaging(int skip, int take)
        {
            Skip = skip;
            Take = take;
            IsPagingEnabled = true;
        }
    }
}
