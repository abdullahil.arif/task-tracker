﻿using TaskTracker.Core.Domain;

namespace TaskTracker.Core
{
    public interface IWorkContext
    {
        string UserId { get; }
        Task<AppUser> GetCurrentUserAsync();
        Task<int> GetCurrentWorkspaceIdAsync();
    }
}
